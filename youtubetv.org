* References

Channel list obtained from here:
- https://tv.youtube.com/welcome/
- Type zip code, submit form

Channel list was obtained on 2019-07-14 for the Raleigh/Durham viewing
area.

In the tables, the "GF Has" column lists:

- 'f' if the channel is included in FiberTV base subscription

- 'f?' if there is uncertainty the channel is same as a FiberTV channel
  in base subscription

- 'no' if FiberTV does not have the channel

- A dollar amount if the channel is a FiberTV premium. Amount of the
  premium package that includes this channel.

* YoutubeTV Channel List

| '# | GF Has | Name                  | Comments                                     |
|----+--------+-----------------------+----------------------------------------------|
|  1 | f      | ABC 11                |                                              |
|  2 | f      | CBS NC                |                                              |
|  3 | f      | Fox 50                |                                              |
|  4 | f      | WRAL                  |                                              |
|  5 | f      | AMC                   |                                              |
|  6 | f      | Animal Planet         |                                              |
|  7 | f      | BBC America           |                                              |
|  8 | f      | BBC World News        |                                              |
|  9 | f      | Bravo                 |                                              |
| 10 | f      | BTN                   | Big Ten Network                              |
| 11 | f      | Cartoon Network       |                                              |
| 12 | no     | CBS Sports Network    |                                              |
| 13 | no     | Cheddar Business      |                                              |
| 14 | no     | Cheddar News          |                                              |
| 15 | f      | CNBC                  |                                              |
| 16 | f      | CNBC World            |                                              |
| 17 | f      | CNN                   |                                              |
| 18 | f      | Comet TV              |                                              |
| 19 | no     | Cozi                  |                                              |
| 20 | f      | CW 22                 |                                              |
| 21 | f      | Discovery Channel     |                                              |
| 22 | f      | Disney Channel        |                                              |
| 23 | f      | Disney Junior         |                                              |
| 24 | f      | Disney XD             |                                              |
| 25 | f      | E!                    |                                              |
| 26 | f      | ESPN                  |                                              |
| 27 | f      | ESPN2                 |                                              |
| 28 | f      | ESPNews               |                                              |
| 29 | f      | ESPNU                 |                                              |
| 30 | f      | Food Network          |                                              |
| 31 | f      | FOX Business          |                                              |
| 32 | f      | FOX News              |                                              |
| 33 | f      | FOX Sports Carolinas  | Fox Sports Carolinas 1                       |
| 34 | f?     | Fox Sports Carolinas  | mabey same as FiberTV "Fox Sports Southeast" |
| 35 | f      | Freeform              |                                              |
| 36 | f      | FS1                   |                                              |
| 37 | f      | FS2                   |                                              |
| 38 | f      | FX                    |                                              |
| 39 | f      | FXM                   |                                              |
| 40 | f      | FXX                   |                                              |
| 41 | f      | Golf Channel          |                                              |
| 42 | f      | HGTV                  |                                              |
| 43 | f      | HLN                   |                                              |
| 44 | f      | ID                    | Investigation Discovery                      |
| 45 | f      | IFC                   |                                              |
| 46 | no     | Local Now             |                                              |
| 47 | no     | MLB Game of the Week  |                                              |
| 48 | f      | MLB Network           |                                              |
| 49 | f      | MotorTrend            |                                              |
| 50 | f      | MSNBC                 |                                              |
| 51 | f      | MyRDCTV               |                                              |
| 52 | f      | Nat Geo               |                                              |
| 53 | f      | Nat Geo Wild          |                                              |
| 54 | no     | NBA TV                |                                              |
| 55 | f      | NBCSN                 | NBC Sports Network                           |
| 56 | f      | Newsy                 |                                              |
| 57 | f      | Olympic Channel       |                                              |
| 58 | f      | Oxygen                |                                              |
| 59 | no     | Pop                   |                                              |
| 60 | f      | SEC Network           | FiberTV has two overflow channels            |
| 61 | $10    | Smithsonian Channel   | In FiberTV package iwth 16 premium channels  |
| 62 | no     | Start TV              |                                              |
| 63 | f      | SundanceTV            |                                              |
| 64 | f      | SYFY                  |                                              |
| 65 | no     | Tastemade             |                                              |
| 66 | f      | TBS                   |                                              |
| 67 | f      | Telemundo 44          |                                              |
| 68 | no     | Tennis Channel        |                                              |
| 69 | f      | TLC                   |                                              |
| 70 | f      | TNT                   |                                              |
| 71 | f      | Travel Channel        |                                              |
| 72 | f      | truTV                 |                                              |
| 73 | f      | Turner Classic Movies |                                              |
| 74 | no     | TYT Network           |                                              |
| 75 | f      | Universal Kids        |                                              |
| 76 | f      | Universo              | NBC Universo en Espanol                      |
| 77 | f      | USA                   |                                              |
| 78 | f      | WE tv                 |                                              |
| 79 | no     | YouTube Originals     |                                              |

* YoutubeTV Premiums

| '# | GF Has | Name             | YoutubeTV Price | Comments                                    |
|----+--------+------------------+-----------------+---------------------------------------------|
|  1 | no     | AMC Premiere     | $5/mo           |                                             |
|  2 | no     | Curiosity Stream | $3/mo           |                                             |
|  3 | no     | EPIX             | $6/mo           |                                             |
|  4 | $10    | Fox Soccer Plus  | $15/mo          | In FiberTV package with 39 premium channels |
|  5 | no     | NBA League Pass  | $40             |                                             |
|  6 | $10    | Showtime         | $7/mo or $11/mo | In FiberTV package with 22 premium channels |
|  7 | no     | Shudder          | $6/mo           |                                             |
|  8 | $10    | Starz            | $9/mo           | In FiberTV package with 34 premium channels |
|  9 | no     | Sundance Now     | $7/mo           |                                             |

# Overview

This project compares the channel list from YoutubeTV and Google Fiber
TV as of 2019-07-14 for the Raleigh/Durham area.

The `.org` files contain a table for each respective service.

* `fiber-tv.org` - Lists the both FiberTV's channels, and channels that
  are missing from FiberTV but are available on YoutubeTV. The
  YoutubeTV-only channels are listed at the top of the table. For each
  table row, a column records if a FiberTV channel is also available in
  YoutubeTV. The montly price of premium channels is noted.

* `youtubetv.org` - Lists channels included in YoutubeTV service. Lists
  available premium channels and their monthly price.

The `raw-data/*.txt` files contain a raw dump of the channel list from
each service's respective web pages for the Raleigh/Durham viewing area.
The data was dumped on 2019-07-14.

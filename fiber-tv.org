* References

FiberTV channel list obtained from here:
- https://fiber.google.com/cities/triangle/channels/
- https://fiber.storage.googleapis.com/channels/guide/triangle.pdf

YoutubeTV channel list obtained from here:
- https://tv.youtube.com/welcome/
- Type zip code, submit form

Both channel lists were obtained on 2019-07-14 for the Raleigh/Durham
viewing area.

In the tables, the "YT Has" column lists:

- 'y' if the channel is included in YoutubeTV base subscription

- 'y?' if there is uncertainty the channel is same as a YoutubeTV
  channel in base subscription

- blank if YoutubeTV does not have the channel

- A dollar amount if the channel is a YoutubeTV premium.

* YoutubeTV Channel Missing From FiberTV

** YoutubeTV Channels Included in Base Subscription

These channels are missing from FiberTV

|    | Channel Name          | Comments |
|----+-----------------------+----------|
|  1 | CBS Sports Network    |          |
|  2 | Cheddar Business      |          |
|  3 | Cheddar News          |          |
|  4 | Cozi TV               |          |
|  5 | Local Now             |          |
|  6 | MLB Game of the Week  |          |
|  7 | NBA TV                |          |
|  8 | POP                   |          |
|  9 | Start TV              |          |
| 10 | Tastemade             |          |
| 11 | Tennis Channel        |          |
| 12 | TYT - The Young Turks |          |
| 13 | YouTube Originals     |          |

** Premium YoutubeTV Channels

These channels are missing from FiberTV

|   | Channel Name     | YT $ | Comments |
|---+------------------+------+----------|
| 1 | AMC Premiere     | $5   |          |
| 2 | Curiosity Stream | $3   |          |
| 3 | EPIX             | $6   |          |
| 4 | NBA League Pass  | $40  |          |
| 5 | Shudder          | $6   |          |
| 6 | Sundance Now     | $7   |          |

* FiberTV Channel List

** Local FiberTV Channels

|    | Channel Name                | YT Has | GF # | FiberTV Package   | Comments |
|----+-----------------------------+--------+------+-------------------+----------|
|  1 | C-SPAN                      |        |  131 | Locals (included) |          |
|  2 | C-SPAN 2                    |        |  132 | Locals (included) |          |
|  3 | C-SPAN 3                    |        |  133 | Locals (included) |          |
|  4 | Cary TV SD                  |        |  142 | Locals (included) |          |
|  5 | Durham Community Channel    |        |    8 | Locals (included) |          |
|  6 | Durham Public Schools       |        |  144 | Locals (included) |          |
|  7 | HSN                         |        |   23 | Locals (included) |          |
|  8 | HSN2                        |        |   24 | Locals (included) |          |
|  9 | Nasa                        |        |  321 | Locals (included) |          |
| 10 | QVC                         |        |   25 | Locals (included) |          |
| 11 | QVC2                        |        |   26 | Locals (included) |          |
| 12 | RTN 10 Public Access SD     |        |  143 | Locals (included) |          |
| 13 | RTN 11 Government Access SD |        |  141 | Locals (included) |          |
| 14 | RTN 18 Education SD         |        |   18 | Locals (included) |          |
| 15 | RTN 22 Bulletin Board SD    |        |  140 | Locals (included) |          |
| 16 | The North Carolina Channel  |        |   78 | Locals (included) |          |
| 17 | WFPXDT (ION Life)           |        |   21 | Locals (included) |          |
| 18 | WLFLDT (The CW)             | y      |   10 | Locals (included) |          |
| 19 | WLFLDT2 (Stadium)           |        |   85 | Locals (included) |          |
| 20 | WLFLDT3 (TBD TV)            |        |   84 | Locals (included) |          |
| 21 | WNCNDT (CBS)                | y      |    6 | Locals (included) |          |
| 22 | WNCNDT3 (Justice Network)   |        |   75 | Locals (included) |          |
| 23 | WRALDT (NBC)                | y      |    5 | Locals (included) |          |
| 24 | WRALDT2 (Heroes & Icons)    |        |   72 | Locals (included) |          |
| 25 | WRAYDT (TCT)                |        |   30 | Locals (included) |          |
| 26 | WRAZDT (FOX)                | y      |   13 | Locals (included) |          |
| 27 | WRAZDT2 (MeTV)              |        |   73 | Locals (included) |          |
| 28 | WRDCDT (My RDC TV)          | y      |   12 | Locals (included) |          |
| 29 | WRDCDT2 (Charge TV)         |        |   86 | Locals (included) |          |
| 30 | WRDCDT3 (Comet)             | y      |   87 | Locals (included) |          |
| 31 | WRPXDT (ION)                |        |   22 | Locals (included) |          |
| 32 | WRPXDT2 (qubo)              |        |   79 | Locals (included) |          |
| 33 | WRTDCD (Telemundo) ES       | y      |   42 | Locals (included) |          |
| 34 | WTVDDT (ABC)                | y      |   11 | Locals (included) |          |
| 35 | WTVDDT2 (Live Well)         |        |   70 | Locals (included) |          |
| 36 | WTVDDT3 (Laff)              |        |   71 | Locals (included) |          |
| 37 | WUNCDT (PBS)                |        |    4 | Locals (included) |          |
| 38 | WUNCDT2 (Rootle)            |        |   76 | Locals (included) |          |
| 39 | WUNCDT3 (UNC Explorer)      |        |   77 | Locals (included) |          |
| 40 | WUVCDT (Univision) ES       |        |   40 | Locals (included) |          |
| 41 | WUVCDT2 (UniMás) ES         |        |   41 | Locals (included) |          |
| 42 | WUVCDT3 (Bounce TV)         |        |   81 | Locals (included) |          |
| 43 | WUVCDT4 (GetTV)             |        |   82 | Locals (included) |          |

** Non-Local FiberTV Channels

|     | Channel Name                    | YT Has | GF # | FiberTV Package | Comments                                              |
|-----+---------------------------------+--------+------+-----------------+-------------------------------------------------------|
|   1 | A&E                             |        |  298 | Included        |                                                       |
|   2 | AMC                             | y      |  288 | Included        |                                                       |
|   3 | American Heroes Channel         |        |  340 | Included        |                                                       |
|   4 | Animal Planet                   | y      |  333 | Included        |                                                       |
|   5 | BBC America                     | y      |  287 | Included        |                                                       |
|   6 | BBC World News                  | y      |  112 | Included        |                                                       |
|   7 | BET                             |        |  355 | Included        |                                                       |
|   8 | BET Gospel SD                   |        |  378 | Included        |                                                       |
|   9 | BET Her                         |        |  356 | Included        |                                                       |
|  10 | BET Jams SD                     |        |  363 | Included        |                                                       |
|  11 | BET Soul SD                     |        |  369 | Included        |                                                       |
|  12 | Boomerang                       |        |  431 | Included        |                                                       |
|  13 | Bravo                           | y      |  296 | Included        |                                                       |
|  14 | BTN – Outer Territory           | y?     |  207 | Included        | YoutubeTV calls it "Big Ten Network"                  |
|  15 | BTN2                            |        |  623 | Included        |                                                       |
|  16 | BTN3                            |        |  624 | Included        |                                                       |
|  17 | BTN4                            |        |  625 | Included        |                                                       |
|  18 | Cartoon Network                 | y      |  351 | Included        |                                                       |
|  19 | Christian Television Network SD |        |  451 | Included        |                                                       |
|  20 | CMT                             |        |  374 | Included        |                                                       |
|  21 | CMT Music SD                    |        |  375 | Included        |                                                       |
|  22 | CNBC                            | y      |  121 | Included        |                                                       |
|  23 | CNBC World SD                   | y      |  122 | Included        |                                                       |
|  24 | CNN                             | y      |  101 | Included        |                                                       |
|  25 | CNN en Espanol SD ES            |        |  462 | Included        |                                                       |
|  26 | CNN International SD            |        |  111 | Included        |                                                       |
|  27 | Comedy Central                  |        |  350 | Included        |                                                       |
|  28 | Cooking Channel                 |        |  393 | Included        |                                                       |
|  29 | Crime & Investigation           |        |  345 | Included        |                                                       |
|  30 | Destination America             |        |  337 | Included        |                                                       |
|  31 | Discovery Channel               | y      |  330 | Included        |                                                       |
|  32 | Discovery Family Channel        |        |  426 | Included        |                                                       |
|  33 | Discovery Life Channel          |        |  335 | Included        |                                                       |
|  34 | Disney                          | y      |  427 | Included        |                                                       |
|  35 | Disney Jr.                      | y      |  429 | Included        |                                                       |
|  36 | Disney XD                       | y      |  428 | Included        |                                                       |
|  37 | DIY                             |        |  394 | Included        |                                                       |
|  38 | E! Entertainment                | y      |  406 | Included        |                                                       |
|  39 | El Rey                          |        |  342 | Included        |                                                       |
|  40 | ESPN                            | y      |  210 | Included        |                                                       |
|  41 | ESPN2                           | y      |  214 | Included        |                                                       |
|  42 | ESPN Deportes ES                |        |  215 | Included        |                                                       |
|  43 | ESPNews                         | y      |  211 | Included        |                                                       |
|  44 | ESPNU                           | y      |  213 | Included        |                                                       |
|  45 | EWTN                            |        |  456 | Included        |                                                       |
|  46 | EWTN en Espanol SD ES           |        |  497 | Included        |                                                       |
|  47 | Food Network                    | y      |  392 | Included        |                                                       |
|  48 | FOX Business News               | y      |  120 | Included        |                                                       |
|  49 | FOX Deportes ES                 |        |  470 | Included        |                                                       |
|  50 | FOX News Channel                | y      |  119 | Included        |                                                       |
|  51 | FOX Sports 1                    | y      |  208 | Included        |                                                       |
|  52 | FOX Sports 2                    | y      |  209 | Included        |                                                       |
|  53 | FOX Sports Carolinas            | y      |  204 | Included        | YoutubeTV's calls it "Fox Sports Carolinas 1"         |
|  54 | FOX Sports Southeast            | y?     |  205 | Included        | Might be same as YoutubeTV's "Fox Sports Carolinas 2" |
|  55 | Freeform                        | y      |  286 | Included        |                                                       |
|  56 | Fusion                          |        |  105 | Included        |                                                       |
|  57 | FX                              | y      |  282 | Included        |                                                       |
|  58 | FX Movie Channel                | y      |  281 | Included        |                                                       |
|  59 | FXX                             | y      |  283 | Included        |                                                       |
|  60 | FYI                             |        |  299 | Included        |                                                       |
|  61 | GAC: Great American Country     |        |  373 | Included        |                                                       |
|  62 | Galavision ES                   |        |  467 | Included        |                                                       |
|  63 | Golf Channel                    | y      |  249 | Included        |                                                       |
|  64 | Hallmark Channel                |        |  291 | Included        |                                                       |
|  65 | Hallmark Movies & Mysteries     |        |  292 | Included        |                                                       |
|  66 | HGTV                            | y      |  391 | Included        |                                                       |
|  67 | Hillsong Channel                |        |  454 | Included        |                                                       |
|  68 | History                         |        |  329 | Included        |                                                       |
|  69 | HLN                             | y      |  102 | Included        |                                                       |
|  70 | IFC                             | y      |  301 | Included        |                                                       |
|  71 | INSP                            |        |  455 | Included        |                                                       |
|  72 | Investigation Discovery         | y      |  339 | Included        |                                                       |
|  73 | Jewish Broadcasting Service     |        |  459 | Included        |                                                       |
|  74 | JUCE TV SD                      |        |  453 | Included        |                                                       |
|  75 | Lifetime                        |        |  399 | Included        |                                                       |
|  76 | Lifetime Movies                 |        |  400 | Included        |                                                       |
|  77 | LOGO                            |        |  295 | Included        |                                                       |
|  78 | LRW SD                          |        |  401 | Included        |                                                       |
|  79 | Military History SD             |        |  322 | Included        |                                                       |
|  80 | MLB Network                     | y      |  217 | Included        |                                                       |
|  81 | MotorTrend                      | y      |  336 | Included        |                                                       |
|  82 | MSNBC                           | y      |  103 | Included        |                                                       |
|  83 | MTV                             |        |  360 | Included        |                                                       |
|  84 | MTV2                            |        |  361 | Included        |                                                       |
|  85 | MTV Classic SD                  |        |  367 | Included        |                                                       |
|  86 | MTV Live                        |        |  370 | Included        |                                                       |
|  87 | MTV Tr3s SD                     |        |  364 | Included        |                                                       |
|  88 | MTVU SD                         |        |  365 | Included        |                                                       |
|  89 | Nat Geo WILD                    | y      |  326 | Included        |                                                       |
|  90 | National Geographic Channel     | y      |  327 | Included        |                                                       |
|  91 | NBC Sports Network              | y      |  203 | Included        | YoutubeTV calls it NBCSN                              |
|  92 | NBC Universo ES                 | y      |  487 | Included        | YoutubeTV calls it "Universo"                         |
|  93 | Newsy                           | y      |  106 | Included        |                                                       |
|  94 | NFL Network                     |        |  219 | Included        |                                                       |
|  95 | Nickelodeon                     |        |  421 | Included        |                                                       |
|  96 | Nick2 SD                        |        |  422 | Included        |                                                       |
|  97 | Nick Jr. SD                     |        |  425 | Included        |                                                       |
|  98 | Nick Music SD                   |        |  362 | Included        |                                                       |
|  99 | Nicktoons                       |        |  423 | Included        |                                                       |
| 100 | Olympic Channel                 | y      |  602 | Included        |                                                       |
| 101 | OWN: Oprah Winfrey              |        |  334 | Included        |                                                       |
| 102 | Oxygen                          | y      |  404 | Included        |                                                       |
| 103 | Paramount Network               |        |  341 | Included        |                                                       |
| 104 | Science Channel                 |        |  331 | Included        |                                                       |
| 105 | SEC Network                     | y      |  216 | Included        |                                                       |
| 106 | SEC Overflow 1                  |        |  617 | Included        |                                                       |
| 107 | SEC Overflow 2                  |        |  618 | Included        |                                                       |
| 108 | Smile of a Child Network SD     |        |  452 | Included        |                                                       |
| 109 | SundanceTV                      | y      |  300 | Included        |                                                       |
| 110 | Syfy                            | y      |  349 | Included        |                                                       |
| 111 | TBN                             |        |  450 | Included        |                                                       |
| 112 | TBN Enlace SD ES                |        |  498 | Included        |                                                       |
| 113 | TBS                             | y      |  284 | Included        |                                                       |
| 114 | TCM: Turner Classic Movies      | y      |  289 | Included        |                                                       |
| 115 | Teen Nick                       |        |  424 | Included        |                                                       |
| 116 | The Word Network SD             |        |  457 | Included        |                                                       |
| 117 | TLC                             | y      |  332 | Included        |                                                       |
| 118 | TNT                             | y      |  285 | Included        |                                                       |
| 119 | Travel Channel                  | y      |  390 | Included        |                                                       |
| 120 | truTV                           | y      |  297 | Included        |                                                       |
| 121 | TV Land                         |        |  290 | Included        |                                                       |
| 122 | Universal Kids                  | y      |  430 | Included        |                                                       |
| 123 | Univision Deportes ES           |        |  468 | Included        |                                                       |
| 124 | USA                             | y      |  280 | Included        |                                                       |
| 125 | VH1                             |        |  368 | Included        |                                                       |
| 126 | Viceland                        |        |  328 | Included        |                                                       |
| 127 | Weather Channel                 |        |   99 | Included        |                                                       |
| 128 | WE tv                           | y      |  402 | Included        |                                                       |
| 129 | WGN America                     |        |  303 | Included        |                                                       |

** Premium FiberTV Channels

The "YT Has" column lists:
- 'y' if the channel is included in the YoutubeTV base subscription
- YoutubeTV price if the channel is a YoutubeTV premium channel

*** Cinemax

|    | Channel Name      | YT Has | GF # | FiberTV Package | Comments |
|----+-------------------+--------+------+-----------------+----------|
|  1 | 5-StarMax East*   |        |  594 | Cinemax $10     |          |
|  2 | ActionMax East*   |        |  588 | Cinemax $10     |          |
|  3 | ActionMax West*   |        |  589 | Cinemax $10     |          |
|  4 | Cinemáx East* ES  |        |  593 | Cinemax $10     |          |
|  5 | Max East*         |        |  584 | Cinemax $10     |          |
|  6 | Max West*         |        |  585 | Cinemax $10     |          |
|  7 | MoreMax East*     |        |  586 | Cinemax $10     |          |
|  8 | MoreMax West*     |        |  587 | Cinemax $10     |          |
|  9 | MovieMax East*    |        |  592 | Cinemax $10     |          |
| 10 | OuterMax East*    |        |  595 | Cinemax $10     |          |
| 11 | ThrillerMax East* |        |  590 | Cinemax $10     |          |
| 12 | ThrillerMax West* |        |  591 | Cinemax $10     |          |

*** Family Package

|    | Channel Name            | YT Has | GF # | FiberTV Package    | Comments                                    |
|----+-------------------------+--------+------+--------------------+---------------------------------------------|
|  1 | Aspire*                 |        |  435 | Family Package $10 |                                             |
|  2 | Baby First*             |        |  433 | Family Package $10 |                                             |
|  3 | BabyTV*                 |        |  432 | Family Package $10 |                                             |
|  4 | DuckTV*                 |        |  437 | Family Package $10 |                                             |
|  5 | FidoTV*                 |        |  436 | Family Package $10 |                                             |
|  6 | Game Show Network East* |        |  352 | Family Package $10 |                                             |
|  7 | Game Show Network West* |        |  353 | Family Package $10 |                                             |
|  8 | Ovation*                |        |  304 | Family Package $10 |                                             |
|  9 | Showtime East*          | $7/$11 |  510 | Family Package $10 | YoutubeTV has "Showtime", $7 might be sale? |
| 10 | Showtime West*          |        |  511 | Family Package $10 |                                             |
| 11 | Showtime Family East*   |        |  518 | Family Package $10 |                                             |
| 12 | Showtime Family West*   |        |  519 | Family Package $10 |                                             |
| 13 | Smithsonian East*       | y      |  320 | Family Package $10 | YoutubeTV has free "Smithsonian Channel"    |
| 14 | Smithsonian West*       |        |  319 | Family Package $10 |                                             |
| 15 | UP*                     |        |  434 | Family Package $10 |                                             |
| 16 | UP Faith & Family*      |        |  VOD | Family Package $10 |                                             |

*** HBO

Not available on YoutubeTV.  Separate HBO Now service (not part of
YoutubeTV) is $15 / month. https://play.hbonow.com/

|    | Channel Name        | YT Has | GF # | FiberTV Package | Comments                          |
|----+---------------------+--------+------+-----------------+-----------------------------------|
|  1 | HBO East*           |        |  570 | HBO $20         | See note above, available for $15 |
|  2 | HBO West*           |        |  571 | HBO $20         |                                   |
|  3 | HBO 2 East*         |        |  572 | HBO $20         |                                   |
|  4 | HBO 2 West*         |        |  573 | HBO $20         |                                   |
|  5 | HBO Comedy East*    |        |  580 | HBO $20         |                                   |
|  6 | HBO Comedy West*    |        |  581 | HBO $20         |                                   |
|  7 | HBO Family East*    |        |  576 | HBO $20         |                                   |
|  8 | HBO Family West*    |        |  577 | HBO $20         |                                   |
|  9 | HBO Latino East* ES |        |  578 | HBO $20         |                                   |
| 10 | HBO Latino West* ES |        |  579 | HBO $20         |                                   |
| 11 | HBO Signature East* |        |  574 | HBO $20         |                                   |
| 12 | HBO Signature West* |        |  575 | HBO $20         |                                   |
| 13 | HBO Zone East*      |        |  582 | HBO $20         |                                   |
| 14 | HBO Zone West*      |        |  583 | HBO $20         |                                   |

*** Latino Package

|    | Channel Name               | YT Has | GF # | FiberTV Package   | Comments |
|----+----------------------------+--------+------+-------------------+----------|
|  1 | Bandamax* SD ES            |        |  490 | Latino Package $5 |          |
|  2 | De Pelicula* SD ES         |        |  484 | Latino Package $5 |          |
|  3 | De Pelicula Clasico* SD ES |        |  485 | Latino Package $5 |          |
|  4 | Discovery en Espanol ES    |        |  474 | Latino Package $5 |          |
|  5 | Discovery Familia ES       |        |  477 | Latino Package $5 |          |
|  6 | FOROtv* SD ES              |        |  463 | Latino Package $5 |          |
|  7 | FOX Life SD ES             |        |  464 | Latino Package $5 |          |
|  8 | GolTV Espanol* ES          |        |  471 | Latino Package $5 |          |
|  9 | History en Espanol* SD ES  |        |  475 | Latino Package $5 |          |
| 10 | Nat Geo Mundo SD ES        |        |  473 | Latino Package $5 |          |
| 11 | Telehit Urbano* SD ES      |        |  491 | Latino Package $5 |          |
| 12 | Telehit* ES                |        |  492 | Latino Package $5 |          |
| 13 | Univision tlnovelas*SD ES  |        |  493 | Latino Package $5 |          |

*** Movies Package

|    | Channel Name        | YT Has | GF # | FiberTV Package    | Comments |
|----+---------------------+--------+------+--------------------+----------|
|  1 | Flix East*          |        |  530 | Movies Package $10 |          |
|  2 | Flix West*          |        |  531 | Movies Package $10 |          |
|  3 | IndiePlex*          |        |  565 | Movies Package $10 |          |
|  4 | MGM*                |        |  601 | Movies Package $10 |          |
|  5 | MoviePlex*          |        |  563 | Movies Package $10 |          |
|  6 | Reelz*              |        |  605 | Movies Package $10 |          |
|  7 | RetroPlex*          |        |  567 | Movies Package $10 |          |
|  8 | Shorts*             |        |  603 | Movies Package $10 |          |
|  9 | Sony Movie Channel* |        |  600 | Movies Package $10 |          |
| 10 | UP Faith & Family*  |        |  VOD | Movies Package $10 |          |

*** Showtime

|    | Channel Name                  | YT Has | GF # | FiberTV Package | Comments |
|----+-------------------------------+--------+------+-----------------+----------|
|  1 | Flix East*                    |        |  530 | Showtime $10    |          |
|  2 | Flix West*                    |        |  531 | Showtime $10    |          |
|  3 | SHO2 East*                    |        |  524 | Showtime $10    |          |
|  4 | SHO2 West*                    |        |  525 | Showtime $10    |          |
|  5 | Showcase East*                |        |  512 | Showtime $10    |          |
|  6 | Showcase West*                |        |  513 | Showtime $10    |          |
|  7 | Showtime Beyond East*         |        |  514 | Showtime $10    |          |
|  8 | Showtime Beyond West*         |        |  515 | Showtime $10    |          |
|  9 | Showtime East*                |        |  510 | Showtime $10    |          |
| 10 | Showtime Extreme East*        |        |  516 | Showtime $10    |          |
| 11 | Showtime Extreme West*        |        |  517 | Showtime $10    |          |
| 12 | Showtime Family East*         |        |  518 | Showtime $10    |          |
| 13 | Showtime Family West*         |        |  519 | Showtime $10    |          |
| 14 | Showtime Next East*           |        |  520 | Showtime $10    |          |
| 15 | Showtime Next West*           |        |  521 | Showtime $10    |          |
| 16 | Showtime West*                |        |  511 | Showtime $10    |          |
| 17 | Showtime Women East*          |        |  522 | Showtime $10    |          |
| 18 | Showtime Women West*          |        |  523 | Showtime $10    |          |
| 19 | The Movie Channel East*       |        |  526 | Showtime $10    |          |
| 20 | The Movie Channel Extra East* |        |  527 | Showtime $10    |          |
| 21 | The Movie Channel Extra West* |        |  528 | Showtime $10    |          |
| 22 | The Movie Channel West*       |        |  529 | Showtime $10    |          |

*** Sports Package

|    | Channel Name                    | YT Has | GF # | FiberTV Package     | Comments |
|----+---------------------------------+--------+------+---------------------+----------|
|  1 | ESPN Classic* SD                |        |  212 | Sports Package◊ $10 |          |
|  2 | ESPN Goal Line/Bases Loaded*    |        |  220 | Sports Package◊ $10 |          |
|  3 | FOX College Sports Atlantic* SD |        |  226 | Sports Package◊ $10 |          |
|  4 | FOX College Sports Central* SD  |        |  227 | Sports Package◊ $10 |          |
|  5 | FOX College Sports Pacific* SD  |        |  228 | Sports Package◊ $10 |          |
|  6 | FOX Soccer Plus*                | $15    |  225 | Sports Package◊ $10 |          |
|  7 | FOX Sports Arizona*             |        |  266 | Sports Package◊ $10 |          |
|  8 | FOX Sports Detroit*             |        |  265 | Sports Package◊ $10 |          |
|  9 | FOX Sports Florida*             |        |  260 | Sports Package◊ $10 |          |
| 10 | FOX Sports Midwest*             |        |  267 | Sports Package◊ $10 |          |
| 11 | FOX Sports Ohio*                |        |  264 | Sports Package◊ $10 |          |
| 12 | FOX Sports San Diego*           |        |  271 | Sports Package◊ $10 |          |
| 13 | FOX Sports Southwest*           |        |  268 | Sports Package◊ $10 |          |
| 14 | FOX Sports Sun*                 |        |  261 | Sports Package◊ $10 |          |
| 15 | FOX Sports West*                |        |  269 | Sports Package◊ $10 |          |
| 16 | FOX Sports Wisconsin*           |        |  273 | Sports Package◊ $10 |          |
| 17 | GolTV English*                  |        |  243 | Sports Package◊ $10 |          |
| 18 | Havoc*                          |        |  246 | Sports Package◊ $10 |          |
| 19 | Longhorn Network*               |        |  230 | Sports Package◊ $10 |          |
| 20 | MavTV*                          |        |  275 | Sports Package◊ $10 |          |
| 21 | MLB Strike Zone*                |        |  218 | Sports Package◊ $10 |          |
| 22 | NFL RedZone*                    |        |  223 | Sports Package◊ $10 |          |
| 23 | Outdoor Channel*                |        |  413 | Sports Package◊ $10 |          |
| 24 | Outside TV*                     |        |  255 | Sports Package◊ $10 |          |
| 25 | PAC-12 Arizona*                 |        |  236 | Sports Package◊ $10 |          |
| 26 | PAC-12 Bay Area*                |        |  232 | Sports Package◊ $10 |          |
| 27 | PAC-12 Los Angeles*             |        |  233 | Sports Package◊ $10 |          |
| 28 | PAC-12 Mountain*                |        |  237 | Sports Package◊ $10 |          |
| 29 | PAC-12 Network*                 |        |  231 | Sports Package◊ $10 |          |
| 30 | PAC-12 Oregon*                  |        |  234 | Sports Package◊ $10 |          |
| 31 | PAC-12 Washington*              |        |  235 | Sports Package◊ $10 |          |
| 32 | Prime Ticket*                   |        |  270 | Sports Package◊ $10 |          |
| 33 | Ride TV*                        |        |  245 | Sports Package◊ $10 |          |
| 34 | Sportsman Channel*              |        |  415 | Sports Package◊ $10 |          |
| 35 | TV Games Network*               |        |  258 | Sports Package◊ $10 |          |
| 36 | TV Games Network 2*             |        |  259 | Sports Package◊ $10 |          |
| 37 | Willow Cricket*                 |        |  244 | Sports Package◊ $10 |          |
| 38 | World Fishing Network*          |        |  256 | Sports Package◊ $10 |          |
| 39 | YES Network*                    |        |  272 | Sports Package◊ $10 |          |

*** Starz

|    | Channel Name                  | YT Has | GF # | FiberTV Package | Comments              |
|----+-------------------------------+--------+------+-----------------+-----------------------|
|  1 | IndiePlex East*               |        |  565 | Starz $10       |                       |
|  2 | IndiePlex West*               |        |  566 | Starz $10       |                       |
|  3 | MoviePlex East*               |        |  563 | Starz $10       |                       |
|  4 | MoviePlex West*               |        |  564 | Starz $10       |                       |
|  5 | RetroPlex East*               |        |  567 | Starz $10       |                       |
|  6 | RetroPlex West*               |        |  568 | Starz $10       |                       |
|  7 | Starz East*                   | $9     |  535 | Starz $10       | YoutubeTV has "Starz" |
|  8 | Starz West*                   |        |  536 | Starz $10       |                       |
|  9 | Starz Cinema East*            |        |  537 | Starz $10       |                       |
| 10 | Starz Cinema West*            |        |  538 | Starz $10       |                       |
| 11 | Starz Comedy East*            |        |  539 | Starz $10       |                       |
| 12 | Starz Comedy West*            |        |  540 | Starz $10       |                       |
| 13 | Starz Edge East*              |        |  541 | Starz $10       |                       |
| 14 | Starz Edge West*              |        |  542 | Starz $10       |                       |
| 15 | Starz Encore East*            |        |  547 | Starz $10       |                       |
| 16 | Starz Encore West*            |        |  548 | Starz $10       |                       |
| 17 | Starz Encore Action East*     |        |  549 | Starz $10       |                       |
| 18 | Starz Encore Action West*     |        |  550 | Starz $10       |                       |
| 19 | Starz Encore Black East*      |        |  551 | Starz $10       |                       |
| 20 | Starz Encore Black West*      |        |  552 | Starz $10       |                       |
| 21 | Starz Encore Classic East*    |        |  557 | Starz $10       |                       |
| 22 | Starz Encore Classic West*    |        |  558 | Starz $10       |                       |
| 23 | Starz Encore Espanol East* ES |        |  553 | Starz $10       |                       |
| 24 | Starz Encore Espanol West* ES |        |  554 | Starz $10       |                       |
| 25 | Starz Encore Family East*     |        |  555 | Starz $10       |                       |
| 26 | Starz Encore Family West*     |        |  556 | Starz $10       |                       |
| 27 | Starz Encore Suspense East*   |        |  559 | Starz $10       |                       |
| 28 | Starz Encore Suspense West*   |        |  560 | Starz $10       |                       |
| 29 | Starz Encore Westerns East*   |        |  561 | Starz $10       |                       |
| 30 | Starz Encore Westerns West*   |        |  562 | Starz $10       |                       |
| 31 | Starz in Black East*          |        |  543 | Starz $10       |                       |
| 32 | Starz in Black West*          |        |  544 | Starz $10       |                       |
| 33 | Starz Kids & Family East*     |        |  545 | Starz $10       |                       |
| 34 | Starz Kids & Family West*     |        |  546 | Starz $10       |                       |

